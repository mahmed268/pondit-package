<?php


namespace pondit\Providers;
use Illuminate\Support\ServiceProvider;

class DataServiceProvider extends ServiceProvider{
    public function register(){

    }
    
    public function boot(){
        $this->loadRoutesFrom(__DIR__.'/../Routes/web.php');
    }
}

?>